$(document).ready(function() {
	tinymce.init({
		selector: '#postData',
		theme: 'modern',
		skin: 'light',
		entity_encoding: 'raw',
		cleanup: true,
		verify_html: true
	});

	$('form').submit(function(e) {
		e.preventDefault();
		tinymce.triggerSave();

		var postTitle = $('#postTitle').val();
		var postData = $('#postData').val();
		var postAuthor = $('#postAuthor').text();

		var ePostData = encodeURIComponent(postData);

		var urlData = 'postTitle=' + postTitle + '&postData=' + ePostData + '&postAuthor=' + postAuthor;

		$.ajax({
			type: 'POST',
			url: 'core/functions/function.new_post.php',
			data: urlData,
			success: function(data) {
				if(data === 'posted') {
					sweetAlert('Success!', "Your post has been created!", 'success');
				} else {
					sweetAlert('Error!', data, 'error');
				}
			}
		});
	});
});