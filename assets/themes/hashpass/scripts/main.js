$(function() {

    $('form').submit(function(e) {
        e.preventDefault();

        var input = $('#input').val();
        var output = $('#output');


        if (input == '') {
            sweetAlert('Error', "Please enter in a password", 'error');
        } else {
            $.ajax({
                type: 'POST',
                url: 'core/functions/function.hash.php',
                data: {
                    hash: input
                },
                success: function(data) {
                    if (data !== '') {
                        //sweetAlert('Success!', "Password successfully hashed!", 'success');
                        output.val(data);
                    }
                }
            });
        }

    });
});
