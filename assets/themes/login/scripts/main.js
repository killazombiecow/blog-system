$(document).ready(function() {
	$('#login-form').submit(function(e) {
		e.preventDefault();
		var username = $('#username').val();
		var password = $('#password').val();

		var loginData = 'username=' + username + '&password=' + password; 

		$.ajax({
			type: 'POST',
			url: 'core/functions/function.login.php',
			data: loginData,
			success: function(data) {
				console.log(data);
				if(data == 'true') {
					window.location.replace('index.php');
				} else {
					if(data === 'false') {
						sweetAlert('Error', 'Incorrect Username/Passowrd', 'error');
					}
				}
			}
		});
	});

});