		<div class="nav">
			<h2>Blog Panel</h2>
			<p>Version <?php echo $config['appVersion']; ?></p>
			<div class="wrap">
				<a href="index.php" class="<?php echo ($page == "home" ? "active" : "")?>"><i class="ti-home"></i> Dashboard</a>
				<a href="newpost.php" class="<?php echo ($page == "newpost" ? "active" : "")?>"><i class="ti-plus"></i> New Post</a>
				<a href="edit.php" class="<?php echo ($page == "edit" ? "active" : "")?>"><i class="ti-pencil"></i> Edit Post</a>
				<a href="hashpassword.php" class="<?php echo ($page == "hash" ? "active" : "")?>"><i class="ti-lock"></i> Hash Password</a>
				<a href="tweet.php" class="<?php echo ($page == "tweet" ? "active" : "")?>"><i class="ti-twitter"></i> Tweetdeck</a>
				<a href="guide.php" class="<?php echo ($page == "guide" ? "active" : "")?>"><i class="ti-layout-grid4-alt"></i> Guide</a>
			</div>
		</div>