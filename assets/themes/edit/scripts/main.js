$(document).ready(function() {
	$('.btn-del').click(function(e) {
		e.preventDefault();

		var delPostID = $(this).attr("data-postID");

		swal({
			title: 'Are you sure?',
			text: "You won't be able to retrieve this post!",
			type: 'warning',
			showCancelButton: true,
			confirmButtonColor: '#DD5561',
			confirmButtonText: "I'm sure",
			closeOnConfirm: false
		}, function() {
			$.ajax({
				type: 'POST',
				url: 'core/functions/function.delpost.php',
				data: {
					id: delPostID
				},
				success: function(data) {
					if(data === 'del') {
						//swal("Success!", "Post " + delPostID + " successfully deleted!", 'success');
						swal({
							title: 'Success!',
							text: "Post " + delPostID + " successfully deleted!",
							type: 'success',
							confirmButtonColor: '#22A86D',
							confirmButtonText: 'Continue',
							closeonComfirm: true
						}, function() {
							location.reload();
						});
					} else {
						if(data === 'error') {
							swal("Error!", "We encountered an error deleting post: " + delPostID, 'error');
						}
					}
				}
			});
		});;
	});
});