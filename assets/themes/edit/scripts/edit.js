$(document).ready(function() {
	tinymce.init({
		selector: '#postData',
		theme: 'modern',
		skin: 'light',
		entity_encoding: 'raw',
		cleanup: true,
		verify_html: true
	});
	$('form').submit(function(e) {
		e.preventDefault();
		tinymce.triggerSave();

		var postTitle = $('#postTitle').val();
		var postData = $('#postData').val();
		var postID = $('#postID').val();

		var epostData = encodeURIComponent(postData);

		var urlData = 'postTitle=' + postTitle + '&postData=' + epostData + '&postID=' + postID;

		$.ajax({
			type: 'POST',
			url: 'core/functions/function.editpost.php',
			data: urlData,
			success: function(data) {
				if(data === 'edit') {
					sweetAlert('Success!', 'You have successfully edited this post!', 'success');
				} else if(data === 'error') {
					sweetAlert('Error!', "We've encountered an error updating this post, please try again later.", 'error');
				}
			}
		});
	});
});