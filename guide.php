<?php $config = include 'core/config/config.php'; session_start(); ?>
<?php 
if(!isset($_SESSION['logged_in']) && $_SESSION['logged_in'] == '') {
	header('Location: login.php');
}
?>
<html lang="en">
	<head>
		<meta charset="utf-8">

		<title><?php echo $config['appTitle'] ?></title>

		<link rel="stylesheet" href="assets/themes/dash/css/main.css">
		<link rel="stylesheet" href="assets/themes/default/css/sweetalert.css">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/themes/default/css/themify-icons.css">
	</head>
	<body>
<?php $page = 'guide'; include 'assets/themes/default/layout/nav.php'; ?>
		<div class="nav-top">
			<div class="right">
				<p>Currently logged in as <?php echo $_SESSION['username']?><a href="logout.php" class="btn">Logout</a></p>
			</div>
		</div>
		<div class="container">
			<div class="header">
				<h2><i class="ti-layout-grid4-alt"></i> How to guide</h2>
			</div>
			<div class="divider"></div>
			<h3>Introduction</h3>
			<p>Blog Panel <?php echo $config['appVersion'] ?> is a panel developed to easily allow you to administer your blog, it introduces basic functions that are needed to run a blog, things such as creating, deleting and editing blog posts.<p>
			<br>
			<h3>How to add an account</h3>
			<p>Creating an account is easy, all you need is access to PhpMyAdmin, and access to the Hash Password page. The first thing you need to do is get the password you want for the account, which can be obtained at the Hash Password page, simply type in the password you want hashed, and copy the output, we'll save that for later. Next, we want to head to our PhpMyAdmin, go to the database you created for blog panel.</p>
			<br>
			<p>You should see something like this: </p>
			<img src="http://i.imsean.me/images/png/4b346f85166141dac83eefba2db7d78a.png" alt="">
			<br>
			<p>Click where it says "Insert" on the users table, and you should be taking to a page that looks like this:</p>
			<br>
			<img src="http://i.imsean.me/images/png/5a7144d70444b1aae6586d2e138a91d2.png" alt="">
			<p>You can now fill out the account data respectively, however make sure that you leave the "id" box empty. Make sure that you're entering the data in the first portion of the page, not the second. When you get to the password box, you are the use the hashed password from earlier. The reason the password is hashed is because, should anyone again unwanted access to the database, passwords aren't viewable in plain text.</p>
		</div>

		<script type="text/javascript" src="assets/themes/default/scripts/jquery-1.11.2.min.js"></script>
		<script type="text/javascript" src="assets/themes/default/scripts/sweetalert.min.js"></script>
	</body>
</html>