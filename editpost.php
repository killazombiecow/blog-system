<?php 
session_start(); 
$config = include 'core/config/config.php'; 
if(!isset($_SESSION['logged_in']) && $_SESSION['logged_in'] == '') {
	header('Location: login.php');
}
?>
<html lang="en">
	<head>
		<meta charset="utf-8">

		<title><?php echo $config['appTitle'] ?></title>

		<link rel="stylesheet" href="assets/themes/dash/css/main.css">
		<link rel="stylesheet" href="assets/themes/edit/css/main.css">
		<link rel="stylesheet" href="assets/themes/default/css/sweetalert.css">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/themes/default/css/themify-icons.css">
	</head>
	<body>
<?php $page = 'edit'; include 'assets/themes/default/layout/nav.php'; ?>
		<div class="nav-top">
			<div class="right">
				<p>Currently logged in as <?php echo $_SESSION['username']?><a href="logout.php" class="btn">Logout</a></p>
			</div>
		</div>
		<div class="container">
			<div class="header">
				<h2><i class="ti-pencil"></i> Editing post <?php echo '#'.$_GET['id']; ?></h2>
			</div>
			<div class="divider"></div>
			<div class="form">
			<?php include 'core/functions/function.getpostdata.php';
			foreach ($queryRes as $post) {
				$postID = $post['id'];
				$postTitle = $post['title'];
				$postData = $post['data'];
				$postAuthor = $post['author'];
				echo '<form action="post">';
				echo '<input type="text" id="postTitle" name="postTitle" placeholder="Post Title" value="' . $postTitle . '">';
				echo '<div class="divider"></div>';
				echo '<textarea name="postData" id="postData">' . $postData . '</textarea>';
				echo '<div class="divider"></div>';
				echo '<input type="hidden" value="' . $postID . '" id="postID">';
				echo '<input type="submit" value="Save">';
			}?>
			</div>
		</div>
		<script type="text/javascript" src="assets/themes/default/scripts/jquery-1.11.2.min.js"></script>
		<script type="text/javascript" src="assets/themes/default/scripts/sweetalert.min.js"></script>
		<script type="text/javascript" src="assets/themes/default/scripts/tinymce/tinymce.min.js"></script>
		<script type="text/javascript" src="assets/themes/edit/scripts/edit.js"></script>
	</body>
</html>