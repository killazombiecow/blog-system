<?php $config = include 'core/config/config.php'; session_start();?>
<!doctype html>
<html lang="en">
	<head>
		<meta charset="utf-8">

		<title><?php echo $config['appTitle']; ?></title>

		<link rel="stylesheet" href="assets/themes/login/css/main.css">
		<link rel="stylesheet" href="assets/themes/default/css/sweetalert.css">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
	</head>
	<body>
		<div class="version">
			<p>Blog Panel Version <?php echo $config['appVersion']; ?></p>
		</div>
		<div class="form">
			<img src="http://teamstronkhaven.com/images/logo.png" alt="TeamStronkHaven Logo">
			<form action="POST" id="login-form">
				<input type="text" id="username" name="username" placeholder="Enter your username">
				<input type="password" id="password" name="password" placeholder="Enter your password">
				<input type="submit" value="Login">
			</form>
		</div>

		<script type="text/javascript" src="assets/themes/default/scripts/jquery-1.11.2.min.js"></script>
		<script type="text/javascript" src="assets/themes/default/scripts/sweetalert.min.js"></script>
		<script type="text/javascript" src="assets/themes/login/scripts/main.js"></script>
	</body>
</html>
