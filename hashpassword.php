<?php $config = include 'core/config/config.php'; session_start(); ?>
<?php 
if(!isset($_SESSION['logged_in']) && $_SESSION['logged_in'] == '') {
	header('Location: login.php');
}
?>
<html lang="en">
	<head>
		<meta charset="utf-8">

		<title><?php echo $config['appTitle'] ?></title>

		<link rel="stylesheet" href="assets/themes/dash/css/main.css">
		<link rel="stylesheet" href="assets/themes/default/css/sweetalert.css">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/themes/default/css/themify-icons.css">
	</head>
	<body>
		<?php $page = 'hash'; include 'assets/themes/default/layout/nav.php'; ?>
		<div class="nav-top">
			<div class="right">
				<p>Currently logged in as <?php echo $_SESSION['username']?><a href="logout.php" class="btn">Logout</a></p>
			</div>
		</div>
		<div class="container">
			<div class="header">
				<h2><i class="ti-lock"></i> Hash a password</h2>
			</div>
			<div class="divider"></div>
			<div class="form">
				<form action="post">
					<center><input type="text" id="input" name="input" placeholder="Enter Password Here"></center>
					<div class="divider"></div>
					<center><input type="text" id="output" name="output" disabled></center>
					<div class="divider"></div>
					<input type="submit" value="Hash Password">
				</form>
			</div>
		</div>

		<p id="postAuthor"><?php echo $_SESSION['username']; ?></p>

		<script type="text/javascript" src="assets/themes/default/scripts/jquery-1.11.2.min.js"></script>
		<script type="text/javascript" src="assets/themes/default/scripts/sweetalert.min.js"></script>
		<script type="text/javascript" src="assets/themes/hashpass/scripts/main.js"></script>
	</body>
</html>