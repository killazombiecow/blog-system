##  Welcome to Blog Panel ##
----------
*Blog Panel is a blog CMS originally designed for a friend but was soon adapted into a much larger project.*

I plan to have Blog Panel turned into a much more organized and complete project, at the moment it is more of a side project that I work on time to time.

#### Planned Features 
 - Full integrated REST API
 - Custom WYSIWYG editor
 - Theme controller/maker
 - Media manager i.e. edit videos, pictures, etc.
 
 


----------
**Blog Panel is still in it's alpha stages, the current build is not ready yet.**

Blog Panel is in need of help! If you would like to contribute to the project, feel free to fork and send a pull request, or contact me directly @ sean@trintondesigns.com and we can work together. 

