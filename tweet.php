<?php $config = include 'core/config/config.php'; session_start(); ?>
<?php 
if(!isset($_SESSION['logged_in']) && $_SESSION['logged_in'] == '') {
	header('Location: login.php');
}
?>
<html lang="en">
	<head>
		<meta charset="utf-8">

		<title><?php echo $config['appTitle'] ?></title>

		<link rel="stylesheet" href="assets/themes/dash/css/main.css">
		<link rel="stylesheet" href="assets/themes/default/css/sweetalert.css">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/themes/default/css/themify-icons.css">
	</head>
	<body>
<?php $page = 'tweet'; include 'assets/themes/default/layout/nav.php'; ?>
		<div class="nav-top">
			<div class="right">
				<p>Currently logged in as <?php echo $_SESSION['username']?><a href="logout.php" class="btn">Logout</a></p>
			</div>
		</div>
		<div class="container">
			<div class="header">
				<h2><i class="ti-twitter"></i> Tweetdeck</h2>
			</div>
			<iframe src="http://tweetdeck.twitter.com" frameborder="0"></iframe>
		</div>

		<script type="text/javascript" src="assets/themes/default/scripts/jquery-1.11.2.min.js"></script>
		<script type="text/javascript" src="assets/themes/default/scripts/sweetalert.min.js"></script>
	</body>
</html>
