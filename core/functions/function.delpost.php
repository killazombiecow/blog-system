<?php
error_reporting(-1);
ini_set('display_errors', 'On');
session_start();
require_once('../config/database.php');
	if(isset($_POST['id']) && $_SERVER['REQUEST_METHOD'] === 'POST') {
		$postID = $_POST['id'];

		$query = getDatabase()->prepare("DELETE FROM posts WHERE id=:id");
		$query->bindParam(':id', $postID);
		$query->execute();

		if($query->execute()) {
			echo 'del';
		} else {
			echo 'error';
		}
	} else {
		echo 'error';
	}
?>