<?php
error_reporting(-1);
ini_set('display_errors', 'On');
session_start();
require_once('../config/database.php');
	if(isset($_POST['postTitle'], $_POST['postData']) && $_SERVER['REQUEST_METHOD'] === 'POST') {
		$postTitle = $_POST['postTitle'];
		$postData = $_POST['postData'];
		$postDate = date('Y-M-D');
		$postAuthor = $_POST['postAuthor'];

		$query = getDatabase()->prepare("INSERT INTO posts (title, data, postdate, author, tags) VALUES(:title, :data, :postdate, :author, :tags)");

		if($query->execute(array(':title' => $postTitle, ':data' => $postData, ':postdate' => $postDate, ':author' => $postAuthor, ':tags' => 'derp'))) {
			echo 'posted';
		} else {
			echo 'error';
		}
	}
?>