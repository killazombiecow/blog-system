<?php
error_reporting(-1);
ini_set('display_errors', 'On');
session_start();
require_once('../config/database.php');
	if(isset($_POST['username'], $_POST['password']) && $_SERVER['REQUEST_METHOD'] === 'POST') {
		$username = strip_tags(htmlentities($_POST['username']));
		$password = strip_tags(htmlentities(sha1($_POST['password'])));

		$query = getDatabase()->prepare("SELECT username, password FROM users WHERE username=:username AND password=:password");
		$query->bindParam(':username', $username);
		$query->bindParam(':password', $password);
		$query->execute();

		$getUser = getDatabase()->prepare("SELECT id, email, username, password, avatar FROM users WHERE username=:username AND password=:password");
		$getUser->bindParam(':username', $username);
		$getUser->bindParam(':password', $password);
		$getUser->execute();

		$userData = $getUser->fetchAll(PDO::FETCH_ASSOC);

		if($row = $query->fetchAll()) {
			foreach($userData as $user) {
				$_SESSION['id'] = $user['id'];
				$_SESSION['email'] = $user['email'];
				$_SESSION['username'] = $user['username'];
				$_SESSION['avatar'] = $user['avatar'];
				$_SESSION['logged_in'] = 'true';
			}

			$setUserStatus = getDatabase()->prepare("UPDATE users SET status='online' WHERE username=:username");
			$setUserStatus->bindParam(':username', $username);
			$setUserStatus->execute();

			echo 'true';
		} else {
			echo 'false';
		}
	}
?>