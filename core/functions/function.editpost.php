<?php
error_reporting(-1);
ini_set('display_errors', 'On');
session_start();
require_once('../config/database.php');
	if(isset($_REQUEST['postID'], $_REQUEST['postTitle'], $_REQUEST['postData'])) {
		$postID = $_REQUEST['postID'];
		$postTitle = $_REQUEST['postTitle'];
		$postData = $_REQUEST['postData'];

		$query = getDatabase()->prepare("UPDATE posts SET title=?, data=? WHERE id=?");

		if($query->execute(array($postTitle, $postData, $postID))) {
			echo 'edit';
		} else {
			echo 'error';
		}
	} else {
		echo 'error';
	} 
?>