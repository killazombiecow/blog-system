<?php
$config = include 'config.php';
function getDatabase() {
	try {
		$config = include 'config.php';
		$username = $config['dbUser'];
		$password = $config['dbPass'];
			$db = new PDO('mysql:host=' . $config["dbHost"] . ';dbname=' . $config['dbName'], $config['dbUser'], $config['dbPass']);
			$db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return $db;
	} catch (PDOException $e) {
		echo "Error connecting to database, please review your database details you specified in the config file"; // Error code 0x03, database connection failed
	}
}
getDatabase();
?>