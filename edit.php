<?php $config = include 'core/config/config.php'; session_start(); ?>
<?php 
if(!isset($_SESSION['logged_in']) && $_SESSION['logged_in'] == '') {
	header('Location: login.php');
}
?>
<html lang="en">
	<head>
		<meta charset="utf-8">

		<title><?php echo $config['appTitle'] ?></title>

		<link rel="stylesheet" href="assets/themes/dash/css/main.css">
		<link rel="stylesheet" href="assets/themes/edit/css/main.css">
		<link rel="stylesheet" href="assets/themes/default/css/sweetalert.css">
		<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
		<link rel="stylesheet" href="assets/themes/default/css/themify-icons.css">
	</head>
	<body>
		<?php $page = 'edit'; include 'assets/themes/default/layout/nav.php'; ?>
		<div class="nav-top">
			<div class="right">
				<p>Currently logged in as <?php echo $_SESSION['username']?><a href="logout.php" class="btn">Logout</a></p>
			</div>
		</div>
		<div class="container edit">
			<div class="header">
				<h2><i class="ti-pencil"></i> Edit a blog post</h2>
			</div>
			<div class="divider"></div>
			<div class="posts">
			<?php include 'core/functions/function.getposts.php';
			foreach ($queryRes as $post) {
				$postID = $post['id'];
				$postTitle = $post['title'];
				$postData = $post['data'];
				$postAuthor = $post['author'];

				if(strlen($postData) >= 200) {
					$postData = substr($postData, 0, 200) . '...';
				}
				if(strlen($postTitle) >= 50) {
					$postTitle = substr($postTitle, 0, 50) . '...';
				}
				echo '<div class="box">';
				echo '<h3>' . $postTitle . ' <small style="opacity: 0.6;">by ' . $postAuthor . '</small></h3>';
				echo '<p class="postData">' . $postData . '</p>';
				echo '<div class="right">';
				echo '<a href="editpost.php?id=' . $postID . '" class="btn-edit">Edit</a>';
				echo '<a href="deletepost.php?id=' . $postID . '" class="btn-del" data-postID="' . $postID . '">Delete</a>';
				echo '</div>';
				echo '</div>';

			}?>
			</div>
		</div>

		<script type="text/javascript" src="assets/themes/default/scripts/jquery-1.11.2.min.js"></script>
		<script type="text/javascript" src="assets/themes/default/scripts/sweetalert.min.js"></script>
		<script type="text/javascript" src="assets/themes/default/scripts/tinymce/tinymce.min.js"></script>
		<script type="text/javascript" src="assets/themes/newpost/scripts/main.js"></script>
		<script type="text/javascript" src="assets/themes/edit/scripts/main.js"></script>
	</body>
</html>